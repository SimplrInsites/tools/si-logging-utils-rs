# SI logging Utilities for Rust

Logging Utilities for Rust built on system logging and env logging. Supporting:

* file logging
* elasticapm
* logstash
* console logging
* formatting