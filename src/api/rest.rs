//! Rest API interaction utilities.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use hyper::{Body, Client, Method, Request, Response};
use hyper::client::HttpConnector;
use serde_json::{to_string, Value};
use hyper::header::HeaderName;
use std::time::Duration;

/// REST Client structure
pub struct RESTClient{
    client: Client<HttpConnector, Body>
}


/// REST client
impl RESTClient {

    /// Build the request
    ///
    /// # Arguments
    /// * `url` - The url to post to
    /// * `post_data` - Post data for the request
    /// * `headers` - Optional headers
    fn build_req(
        url: &str,
        post_data: HashMap<String, Value>,
        headers: Option<HashMap<String, Vec<String>>>) -> Result<Request<Body>, ()> {
        let mut req_builder = Request::builder()
            .method(Method::POST)
            .uri(url);
        if headers.is_some() {
            for (header, vals) in headers.unwrap() {
                for val in vals {
                    let header_name = HeaderName::from_lowercase(
                        header.to_ascii_lowercase().as_bytes());
                    if header_name.is_ok() {
                        req_builder = req_builder.header(header_name.ok().unwrap(), val);
                    }
                }
            }
        }
        let post_json = to_string(&post_data).ok().unwrap();
        let body = Body::from(post_json);
        let req_result = req_builder.body(body);
        if req_result.is_ok() {
            let r = req_result.ok().unwrap();
            Ok(r)
        } else {
            Err(())
        }
    }


    /// Post to a web server
    ///
    /// # Arguments
    /// * `url` - The url to post to
    /// * `post_data` - Post data for the request
    /// * `headers` - Optional headers
    pub async fn post(
        client: Client<HttpConnector, Body>,
        url: &str,
        post_data: HashMap<String, Value>,
        headers: Option<HashMap<String, Vec<String>>>) -> Result<Response<Body>, ()> {
        let req_result = RESTClient::build_req(url, post_data, headers);
        if req_result.is_ok() {
            let req = req_result.ok().unwrap();
            let response_result = client.request(req).await;
            if response_result.is_ok(){
                let response= response_result.ok().unwrap();
                Ok(response)
            }else {
                Err(())
            }
        } else {
            Err(())
        }
    }

    pub fn new(
        pool_timeout: Option<u64>, http2_only: Option<bool>, retry_request: Option<bool>) -> RESTClient{
        let millis: u64 =  pool_timeout.unwrap_or(32).into();
        let client = Client::builder()
            .pool_idle_timeout(Duration::from_millis(millis))
            .http2_only(http2_only.unwrap_or(true))
            .retry_canceled_requests(retry_request.unwrap_or(false))
            .build_http();
        RESTClient{
            client
        }
    }
}


#[cfg(test)]
mod tests{
    use tokio::runtime::Runtime;

    use super::*;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all()
            .build();
        rt.ok().unwrap()
    }

    #[test]
    fn should_send_post_request(){
        let mut rt = get_runtime();
        rt.block_on(async{
            let url = "http://127.0.0.1:8200/intake/v2/events";
            let pdata = HashMap::<String, Value>::new();
        });
    }

    #[test]
    fn should_send_post_request_in_spawn(){

    }
}