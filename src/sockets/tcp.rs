//! TCP sockets for connecting directly to logging servers over tcp
//!
//! ---
//! author: Andrew Evans
//! ---

use tokio::net::TcpStream;
use tokio::prelude::*;
use std::net::{ToSocketAddrs, Shutdown};


/// Send a message over a tcp connection asynchronously. Returns true if the operation proceeds.
///
/// # Arguments
/// * `msg` - The message to send
/// * `host` - The host address
/// * `port` - The port to send to
pub async fn send_message_async(msg: &str, host: &str, port: i32) -> Result<bool, ()>{

    let url = format!("{}:{}", host, port);
    let addr = url.to_socket_addrs().unwrap().next().unwrap();
    let stream_result = TcpStream::connect(addr).await;
    if stream_result.is_ok(){
        let mut stream = stream_result.unwrap();
        let (_, mut send) = stream.split();
        let result = send.write_all(msg.as_bytes()).await;
        if result.is_ok(){
            let flush_result = send.flush().await;
            if flush_result.is_ok() {
                let _r = stream.shutdown(Shutdown::Both);
                Ok(true)
            }else{
                Err(())
            }
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


#[cfg(test)]
mod test{
    use super::*;
    use tokio::runtime::Runtime;

    fn get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new()
            .enable_all()
            .core_threads(4)
            .threaded_scheduler()
            .enable_all()
            .build();
        rt.ok().unwrap()
    }

    #[test]
    fn test_send_to_vector(){
        let mut rt = get_runtime();
        let msg = "rust test message";
        let _r = rt.block_on(async {
            let host= "127.0.0.1";
            let port = 5961;
            let r= send_message_async(msg, host, port).await;
            assert!(r.is_ok());
        });
    }

    #[test]
    fn test_spawn_send(){
        let mut rt = get_runtime();
        let msg = "rust test message";
        let handle = rt.spawn(async move{
            let host= "127.0.0.1";
            let port = 5961;
            let r= send_message_async(msg, host, port).await;
            assert!(r.is_ok());
        });
        let _r = rt.block_on(async move{
           handle.await
        });
    }

    #[test]
    fn test_send_multiple_msgs(){
        let mut rt = get_runtime();
        let mut handles = vec![];
        for i in 0..10 {
            let msg = "rust test message";
            let handle = rt.spawn(async move {
                let host = "127.0.0.1";
                let port = 5961;
                let r = send_message_async(msg, host, port).await;
                assert!(r.is_ok());
            });
            handles.push(handle);
        }
        rt.block_on(async move{
            for handle in handles {
                let _r = handle.await;
            }
        });
    }
}