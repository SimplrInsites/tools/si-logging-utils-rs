//! Configuration for pushing vector to tcp
//!
//! ---
//! author: Andrew Evans
//! ---


/// Vector configuration
#[derive(Builder)]
struct VectorConfig{
    host: &'static str,
    port: i32
}


/// Implementation of the vector configuration
impl VectorConfig{

    /// Get the port `std::i32`
    #[allow(dead_code)]
    pub fn get_port(&self) -> i32 {
        self.port
    }

    /// Get the host string reference
    #[allow(dead_code)]
    pub fn get_host(&self) -> &'static str {
        self.host
    }
}



#[cfg(test)]
mod test{
    use super::*;

    #[test]
    fn test_build_config(){
        let builder_res = VectorConfigBuilder::default().host("127.0.0.1").port(5961).build();
        assert!(builder_res.is_ok());
        let builder = builder_res.ok().unwrap();
        assert!(builder.host.eq("127.0.0.1"));
        assert_eq!(builder.port, 5961);
    }
}

