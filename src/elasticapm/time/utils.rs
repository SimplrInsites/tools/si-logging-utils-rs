//! Utilties to get timestamps
//!
//! ---
//! author: Andrew Evans
//! ---

use chrono::Utc;


/// Get the number of microseconds since the epoch
fn get_time_epoch() -> i64 {
    Utc::now().timestamp_nanos() / 1000
}


/// Get the date in rfc3339 format. Used for transactions, for instance.
fn get_date_rfc3339() -> String{
    Utc::now().to_rfc3339()
}
