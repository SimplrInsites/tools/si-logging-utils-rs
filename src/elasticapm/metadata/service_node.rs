//! A service node
//!
//! ---
//! author: Andrew Evans
//! ---

use std::env;
use serde::Serialize;

use uuid::Uuid;


/// Service node information
#[derive(Builder, Clone, Serialize)]
pub struct NodeMeta{
    #[builder(default = "self.get_name_default()")]
    configured_name: String
}


/// Node Builder implementation
impl NodeMetaBuilder{

    /// Get the configured name default
    fn get_name_default(&self) -> String{
        let r = env::var("KUBERNETES_NODE_NAME");
        if r.is_ok(){
            r.ok().unwrap()
        }else {
            let id= Uuid::new_v4();
            format!("{}", id)
        }
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_create_node_with_defaults(){
        let r = NodeMetaBuilder::default().build();
        assert!(r.is_ok());
        let meta: NodeMeta = r.ok().unwrap();
        assert!(!meta.configured_name.eq(""));
    }

    #[test]
    fn should_create_node(){
        let r = NodeMetaBuilder::default()
            .configured_name("testname".to_string())
            .build();
        assert!(r.is_ok());
        let meta: NodeMeta = r.ok().unwrap();
        assert!(meta.configured_name.eq("testname"));
    }

    #[test]
    fn should_serialize_node(){
        let r = NodeMetaBuilder::default()
            .configured_name("testname".to_string())
            .build();
        assert!(r.is_ok());
        let meta: NodeMeta = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}
