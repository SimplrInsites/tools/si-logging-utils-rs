//! Language information
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;
use version_check::Version;


/// Language information
#[derive(Clone, Serialize)]
pub struct LanguageMeta{
    name: String,
    version: String
}


/// Language implementation
impl LanguageMeta{

    /// Create a new language
    pub fn new() -> LanguageMeta{
        let mut lang = "0.0".to_string();
        let v = Version::read();
        if v.is_some(){
            lang = format!("{}", v.unwrap());
        }
        LanguageMeta{
            name: "rust".to_string(),
            version: lang
        }
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_create_language_with_version(){
        let lang = LanguageMeta::new();
        assert!(lang.name.eq("rust"));
        assert!(!lang.version.eq("0.0"));
    }

    #[test]
    fn should_serialize_language(){
        let lang = LanguageMeta::new();
        let r = serde_json::to_string(&lang);
        assert!(r.is_ok());
        println!("{}", r.unwrap());
    }
}