//! System information for metadata
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;
use std::env;

use crate::elasticapm::metadata::kubernetes::KubernetesMeta;


#[derive(Builder, Clone, Serialize)]
pub struct ContainerMeta{
    id: String
}


/// System Metadata
#[derive(Builder, Clone, Serialize)]
pub struct SystemMeta{
    #[builder(default = "self.get_arch()")]
    architecture: String,
    #[builder(default = "self.get_hostname()")]
    detected_hostname: String,
    #[builder(default = "self.get_hostname()")]
    configured_hostname: String,
    #[builder(default = "self.get_platform()")]
    platform: String,
    #[builder(default = "self.get_container_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    container: Option<ContainerMeta>,
    #[builder(default = "self.get_kubernetes_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    kubernetes: Option<KubernetesMeta>,
}


/// System Meta implementation
impl SystemMetaBuilder{

    /// Get the platform
    fn get_platform(&self) -> String{
        let platform = os_type::current_platform().os_type;
        format!("{:?}", platform)
    }

    /// Get the none default
    fn get_container_default(&self) -> Option<ContainerMeta>{
        None
    }

    /// Get the none default
    fn get_kubernetes_default(&self) -> Option<KubernetesMeta>{
        None
    }

    /// Get the architecture
    fn get_arch(&self) -> String{
        env::consts::ARCH.to_string()
    }

    /// Get the hostname
    fn get_hostname(&self) -> String{
        let host= gethostname::gethostname();
        let name = host.to_str();
        if name.is_some(){
            name.unwrap().to_string()
        }else{
            "agent_host".to_string()
        }
    }
}


#[cfg(test)]
pub mod tests{
    use super::*;
    use crate::elasticapm::metadata::kubernetes::KubernetesMetaBuilder;

    #[test]
    fn should_serialize_container_skipping_nones(){
        let sm = SystemMetaBuilder::default().build().unwrap();
        let jstrr = serde_json::to_string(&sm);
        let jstr = jstrr.ok().unwrap();
        println!("{}", jstr);
    }

    #[test]
    fn should_serialize_with_non_defaults(){
        let mut b = SystemMetaBuilder::default();
        b.architecture("myarch".to_string());
        let r = b.build();
        let sm: SystemMeta = r.ok().unwrap();
        assert!(sm.architecture.eq("myarch"));
        let jstrr = serde_json::to_string(&sm);
        let jstr = jstrr.ok().unwrap();
        println!("{}", jstr);
    }

    #[test]
    fn should_serialize_with_container(){
        let cr = ContainerMetaBuilder::default()
            .id("1abc2".to_string())
            .build();
        let container = cr.ok().unwrap();
        let mut b = SystemMetaBuilder::default();
        b.architecture("myarch".to_string());
        b.container(Some(container));
        let r = b.build();
        let sm: SystemMeta = r.ok().unwrap();
        assert!(sm.architecture.eq("myarch"));
        let jstrr = serde_json::to_string(&sm);
        let jstr = jstrr.ok().unwrap();
        println!("{}", jstr);
    }

    #[test]
    fn should_serialize_with_kubernetes(){
        let cr = ContainerMetaBuilder::default()
            .id("1abc2".to_string())
            .build();
        let kb = KubernetesMetaBuilder::default().build();
        let kubernetes = kb.ok().unwrap();
        let container = cr.ok().unwrap();
        let mut b = SystemMetaBuilder::default();
        b.architecture("myarch".to_string());
        b.container(Some(container));
        b.kubernetes(Some(kubernetes));
        let r = b.build();
        let sm: SystemMeta = r.ok().unwrap();
        assert!(sm.architecture.eq("myarch"));
        let jstrr = serde_json::to_string(&sm);
        let jstr = jstrr.ok().unwrap();
        println!("{}", jstr);
    }
}
