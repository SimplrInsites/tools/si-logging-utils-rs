//! Pod used to get information
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;

use std::env;


/// Pod information
#[derive(Builder, Clone, Serialize)]
pub struct PodMeta{
    #[builder(default = "self.get_uid_default()")]
    uid: String,
    #[builder(default = "self.get_name_default()")]
    name: String
}


/// Pod builder implementation
impl PodMetaBuilder{

    /// Get the uid default
    fn get_uid_default(&self) -> String{
        let r = env::var(
            "KUBERNETES_POD_UID");
        if r.is_ok(){
            r.ok().unwrap()
        }else{
            "".to_string()
        }
    }

    /// Get the name default
    fn get_name_default(&self) -> String{
        let r = env::var(
            "KUBERNETES_POD_NAME");
        if r.is_ok(){
            r.ok().unwrap()
        }else{
            "".to_string()
        }
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_get_pod_with_env_vars(){
        env::set_var("KUBERNETES_POD_NAME", "TESTNAME");
        env::set_var("KUBERNETES_POD_UID", "TESTUID");
        let pr = PodMetaBuilder::default().build();
        assert!(pr.is_ok());
        let pod: PodMeta = pr.ok().unwrap();
        assert!(pod.name.eq("TESTNAME"));
        assert!(pod.uid.eq("TESTUID"));
    }

    #[test]
    fn should_serialize_pod(){
        env::set_var("KUBERNETES_POD_NAME", "TESTNAME");
        env::set_var("KUBERNETES_POD_UID", "TESTUID");
        let pr = PodMetaBuilder::default().build();
        assert!(pr.is_ok());
        let pod: PodMeta = pr.ok().unwrap();
        let jstr = serde_json::to_string(&pod);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }

    #[test]
    fn should_not_fail_if_envs_not_present(){
        let pr = PodMetaBuilder::default().build();
        assert!(pr.is_ok());
        let pod: PodMeta = pr.ok().unwrap();
        let jstr = serde_json::to_string(&pod);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}
