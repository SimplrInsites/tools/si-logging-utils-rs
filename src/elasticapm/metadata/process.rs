//! Process metadata
//!
//! ---
//! author: Andrew Evans
//! ---

use std::env;

use serde::Serialize;
use sysinfo::{ProcessExt, System, SystemExt};


/// Process metadata
#[derive(Clone, Serialize)]
pub struct ProcessMeta{
    pid: usize,
    title: String,
    ppid: usize,
    argv: Vec<String>
}


/// Process metadata
impl ProcessMeta{

    /// Get the parent pid
    ///
    /// # Arguments
    /// * `pid` - Process pid to return the parent id of
    fn get_ppid(pid: usize) -> usize{
        let s = System::new_all();
        let p = s.get_process(pid);
        if p.is_some() {
            let ppid = p.unwrap().parent();
            if ppid.is_some(){
                ppid.unwrap()
            }else{
                0
            }
        }else{
            0
        }
    }

    /// Get metadata related to the current process
    fn get_meta() -> Result<ProcessMeta, ()>{
        let pr = env::current_exe();
        let mut title = "".to_string();
        if pr.is_ok(){
            let pbuf = pr.ok().unwrap();
            let pstr = pbuf.to_str().unwrap();
            title = pstr.to_string();
        }
        let pidr = sysinfo::get_current_pid();
        if pidr.is_ok(){
            let pid = pidr.ok().unwrap();
            let ppid = ProcessMeta::get_ppid(pid);
            let args = env::args();
            let mut argv = vec![];
            for arg in args{
                argv.push(arg)
            }
            let meta = ProcessMeta{
                pid,
                title,
                ppid,
                argv
            };
            Ok(meta)
        }else{
            Err(())
        }

    }

    /// Get a new process meta. Built from standard library discovery and os.
    pub fn new() -> Result<ProcessMeta, ()>{
        ProcessMeta::get_meta()
    }
}


#[cfg(test)]
pub mod tests{
    use crate::elasticapm::metadata::process::ProcessMeta;
    use sysinfo;
    use sysinfo::{ProcessExt, System, SystemExt};

    #[test]
    fn test_get_parent_id(){
        let pid = sysinfo::get_current_pid();
        let upid = pid.ok().unwrap();
        let ppid = ProcessMeta::get_ppid(upid);
        assert!(ppid > 0);
    }

    #[test]
    fn test_should_get_system_info(){
        let metar = ProcessMeta::get_meta();
        assert!(metar.is_ok());
        let meta = metar.ok().unwrap();
        let pidr = sysinfo::get_current_pid();
        let pid = pidr.ok().unwrap();
        let s = System::new_all();
        let p = s.get_process(pid);
        assert!(p.is_some());
        let ppido = p.unwrap().parent();
        assert!(ppido.is_some());
        let ppid = ppido.unwrap();
        assert_eq!(meta.pid, pid);
        assert_eq!(meta.ppid, ppid);
    }

    #[test]
    fn test_should_convert_to_json(){
        let metar = ProcessMeta::get_meta();
        let meta = metar.ok().unwrap();
        let jstr= serde_json::to_string(&meta);
        println!("{}", jstr.ok().unwrap());
    }
}
