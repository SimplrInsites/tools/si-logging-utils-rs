//! Framework Metadata
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;


/// Framework metadata
#[derive(Builder, Clone, Serialize)]
pub struct FrameworkMeta{
    name: String,
    version: String
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_create_framework_meta(){
        let r = FrameworkMetaBuilder::default()
            .name("testname".to_string())
            .version("1.0".to_string())
            .build();
        let meta: FrameworkMeta = r.ok().unwrap();
        assert!(meta.name.eq("testname"));
        assert!(meta.version.eq("1.0"));
    }

    #[test]
    fn should_serialize_framework_meta(){
        let r = FrameworkMetaBuilder::default()
            .name("testname".to_string())
            .version("1.0".to_string())
            .build();
        let meta: FrameworkMeta = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}
