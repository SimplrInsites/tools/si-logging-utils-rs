//! Agent for the metadata
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;
use uuid::Uuid;


/// Agent information
#[derive(Clone, Serialize)]
pub struct AgentMeta{
    version: String,
    name: String,
    ephemeral_id: String
}


/// Agent implementation
impl AgentMeta{


    fn get_name_default() -> String{
        "rust".to_string()
    }


    /// Get the version default
    fn get_version_default() -> String{
        "0.1b".to_string()
    }

    /// Get a default ephemeral id
    fn get_id_default() -> String{
        let uid = Uuid::new_v4();
        format!("{}", uid)
    }

    /// Constructor for the AgentMeta.
    pub fn new() -> AgentMeta{
        let ephemeral_id = AgentMeta::get_id_default();
        let version = AgentMeta::get_version_default();
        let name = AgentMeta::get_name_default();
        AgentMeta{
            version,
            name,
            ephemeral_id
        }
    }
}


#[cfg(test)]
mod test{
    use crate::elasticapm::metadata::agent::AgentMeta;

    #[test]
    fn should_create_agent_with_defaults(){
        let meta: AgentMeta = AgentMeta::new();
        assert!(meta.version.eq("0.1b"));
        assert!(meta.name.eq("rust"));
        assert!(meta.ephemeral_id.len() > 0);
    }

    #[test]
    fn should_serialize_agent(){
        let meta: AgentMeta = AgentMeta::new();
        let r = serde_json::to_string(&meta);
        assert!(r.is_ok());
        println!("{}", r.unwrap());
    }
}
