//! A metadata builder for the elastic APM
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;

use crate::elasticapm::metadata::process::ProcessMeta;
use crate::elasticapm::metadata::system::{SystemMeta, SystemMetaBuilder};
use crate::elasticapm::metadata::service::ServiceMeta;
use crate::elasticapm::metadata::user::UserMeta;
use std::collections::HashMap;
use serde_json::Value;


/// APM Metadata information. Only the service must be set.
#[derive(Builder, Clone, Serialize)]
pub struct APMMetadata{
    #[builder(default = "self.get_process_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    process: Option<ProcessMeta>,
    #[builder(default = "self.get_system_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    system: Option<SystemMeta>,
    service: ServiceMeta,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_user_default()")]
    user: Option<UserMeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_labels_default()")]
    labels: Option<HashMap<String, Value>>
}


/// Builder implementation
impl APMMetadataBuilder{

    fn get_process_default(&self) -> Option<ProcessMeta>{
        let r = ProcessMeta::new();
        assert!(r.is_ok());
        let meta = r.ok().unwrap();
        Some(meta)
    }

    fn get_system_default(&self) -> Option<SystemMeta>{
        let meta = SystemMetaBuilder::default().build().unwrap();
        Some(meta)
    }

    fn get_user_default(&self) -> Option<UserMeta>{
        None
    }

    fn get_labels_default(&self) -> Option<HashMap<String, Value>>{
        None
    }
}


#[cfg(test)]
pub mod test{
    use super::*;
    use crate::elasticapm::metadata::service::ServiceMetaBuilder;
    use serde_json::Value;

    #[test]
    fn should_serialize_with_defaults(){
        let sr = ServiceMetaBuilder::default()
            .name("TESTAPP".to_string())
            .environment(Some("test".to_string()))
            .build();
        let service = sr.ok().unwrap();
        let r = APMMetadataBuilder::default()
            .service(service)
            .build();
        let meta: APMMetadata = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }

    #[test]
    fn should_serialize_with_labels(){
        let mut labels = HashMap::<String, Value>::new();
        labels.insert("test".to_string(), Value::String("val".to_string()));
        let sr = ServiceMetaBuilder::default()
            .name("TESTAPP".to_string())
            .environment(Some("test".to_string()))
            .build();
        let service = sr.ok().unwrap();
        let r = APMMetadataBuilder::default()
            .service(service)
            .labels(Some(labels))
            .build();
        let meta: APMMetadata = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}