//! Node information for kubernetes
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;

use std::env;


/// Node information
#[derive(Builder, Clone, Serialize)]
pub struct NodeMeta {
    #[builder(default = "self.get_name_default()")]
    name: String
}


/// Node meta implementation
impl NodeMetaBuilder{

    /// Get the name metadata
    fn get_name_default(&self) -> String{
        let r = env::var("KUBERNETES_NODE_NAME");
        if r.is_ok(){
            r.ok().unwrap()
        }else{
            "".to_string()
        }
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_get_node_with_defaults(){
        env::set_var("KUBERNETES_NODE_NAME", "testname");
        let r = NodeMetaBuilder::default().build();
        assert!(r.is_ok());
        let meta: NodeMeta = r.unwrap();
        assert!(meta.name.eq("testname"));
    }

    #[test]
    fn should_serialize_node(){
        env::set_var("KUBERNETES_NODE_NAME", "testname");
        let r = NodeMetaBuilder::default().build();
        assert!(r.is_ok());
        let meta: NodeMeta = r.unwrap();
        assert!(meta.name.eq("testname"));
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }

    #[test]
    fn should_serialize_if_envs_not_set(){
        let r = NodeMetaBuilder::default().build();
        assert!(r.is_ok());
        let meta: NodeMeta = r.unwrap();
        assert!(meta.name.eq(""));
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}