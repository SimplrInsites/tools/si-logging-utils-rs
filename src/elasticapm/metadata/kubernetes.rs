//! Kubernetes setup
//!
//! ---
//! author: Andrew Evans
//! ---


use serde::Serialize;

use std::env;

use crate::elasticapm::metadata::pod::{PodMeta, PodMetaBuilder};
use crate::elasticapm::metadata::kubernetes_node::{NodeMeta, NodeMetaBuilder};


/// Kubernetes Structure
#[derive(Builder, Clone, Serialize)]
pub struct KubernetesMeta{
    #[builder(default = "self.get_namespace_default()")]
    namespace: String,
    #[builder(default = "self.get_pod_default()")]
    pod: PodMeta,
    #[builder(default = "self.get_node_default()")]
    node: NodeMeta
}


/// Meta builder implementation
impl KubernetesMetaBuilder{

    /// Get the default kubernetes namespace or `default`
    fn get_namespace_default(&self) -> String{
        let r = env::var("KUBERNETES_NAMESPACE");
        if r.is_ok(){
            r.ok().unwrap()
        }else{
            "default".to_string()
        }
    }

    /// Get a default pod
    fn get_pod_default(&self) -> PodMeta{
        let r = PodMetaBuilder::default().build();
        assert!(r.is_ok());
        r.ok().unwrap()
    }

    /// Get a node default
    fn get_node_default(&self) -> NodeMeta{
        let r = NodeMetaBuilder::default().build();
        assert!(r.is_ok());
        r.ok().unwrap()
    }
}


#[cfg(test)]
pub mod tests{
    use super::*;
    use crate::elasticapm::metadata::kubernetes_node::NodeMetaBuilder;


    #[test]
    fn sould_create_kubernetes_meta_with_defaults(){
        let r = KubernetesMetaBuilder::default().build();
        let meta: KubernetesMeta = r.ok().unwrap();
        assert!(meta.namespace.eq("default"))
    }

    #[test]
    fn should_set_kubernetes_node(){
        let mut nb = NodeMetaBuilder::default();
        nb.name("testname".to_string());
        let node = nb.build().unwrap();
        let mut kb = KubernetesMetaBuilder::default();
        kb.node(node);
        let r = kb.build();
        let meta: KubernetesMeta = r.ok().unwrap();
        assert!(meta.namespace.eq("default"));
    }

    #[test]
    fn should_create_kubernetes_meta_with_namespace(){
        let mut nb = NodeMetaBuilder::default();
        nb.name("testname".to_string());
        let node = nb.build().unwrap();
        let mut kb = KubernetesMetaBuilder::default();
        kb.node(node);
        kb.namespace("testspace".to_string());
        let r = kb.build();
        let meta: KubernetesMeta = r.ok().unwrap();
        assert!(meta.namespace.eq("testspace"));
    }

    #[test]
    fn should_serialize_kubernetes_meta(){
        let mut nb = NodeMetaBuilder::default();
        nb.name("testname".to_string());
        let node = nb.build().unwrap();
        let mut kb = KubernetesMetaBuilder::default();
        kb.node(node);
        kb.namespace("testspace".to_string());
        let r = kb.build();
        let meta: KubernetesMeta = r.ok().unwrap();
        assert!(meta.namespace.eq("testspace"));
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.ok().unwrap());
    }

}
