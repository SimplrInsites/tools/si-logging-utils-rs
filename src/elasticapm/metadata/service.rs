//! Service information
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;

use crate::elasticapm::metadata::language::LanguageMeta;
use crate::elasticapm::metadata::agent::AgentMeta;
use crate::elasticapm::metadata::service_node::{NodeMeta, NodeMetaBuilder};
use crate::elasticapm::metadata::framework::FrameworkMeta;


/// Service Metadata
#[derive(Builder, Clone, Serialize)]
pub struct ServiceMeta{
    name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_version_default()")]
    version: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_node_default()")]
    node: Option<NodeMeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_environment_default()")]
    environment: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_language_default()")]
    language: Option<LanguageMeta>,
    #[builder(default = "self.get_agent_default()")]
    agent: AgentMeta,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_framework_default()")]
    framework: Option<FrameworkMeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_language_default()")]
    runtime: Option<LanguageMeta>
}


/// Service meta builder
impl ServiceMetaBuilder{

    /// Get the version values, none
    fn get_version_default(&self) -> Option<String>{
        None
    }

    /// Get the environment default, none
    fn get_environment_default(&self) -> Option<String>{
        None
    }

    /// Get the default node values
    fn get_node_default(&self) -> Option<NodeMeta>{
        let r = NodeMetaBuilder::default().build();
        let meta = r.ok().unwrap();
        Some(meta)
    }

    /// Get language and runtime defaults
    fn get_language_default(&self) -> Option<LanguageMeta>{
        let meta = LanguageMeta::new();
        Some(meta)
    }

    /// Get Agent meta
    fn get_agent_default(&self) -> AgentMeta{
        AgentMeta::new()
    }

    /// Get the framework meta default, none
    fn get_framework_default(&self) -> Option<FrameworkMeta> {
        None
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_build_service_meta_with_defaults(){
        let r = ServiceMetaBuilder::default()
            .name("TESTAPP".to_string())
            .environment(Some("test".to_string()))
            .build();
        let meta: ServiceMeta = r.ok().unwrap();
        assert!(meta.name.eq("TESTAPP"));
        assert!(meta.environment.is_some());
        assert!(meta.environment.unwrap().eq("test"));
    }

    #[test]
    fn should_serialize_service(){
        let r = ServiceMetaBuilder::default()
            .name("TESTAPP".to_string())
            .environment(Some("test".to_string()))
            .build();
        let meta: ServiceMeta = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.ok().unwrap());
    }
}