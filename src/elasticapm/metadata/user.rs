//! User metadata
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;


#[derive(Clone, Builder, Serialize)]
pub struct UserMeta{
    #[builder(default = "self.get_str_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    id: Option<String>,
    #[builder(default = "self.get_str_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    email: Option<String>,
    #[builder(default = "self.get_str_default()")]
    #[serde(skip_serializing_if = "Option::is_none")]
    username: Option<String>
}


/// Implementation of meta builder
impl UserMetaBuilder{

    /// Get the default for all values, none
    fn get_str_default(&self) -> Option<String>{
        None
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_create_user(){
        let r = UserMetaBuilder::default()
            .id(Some("myid".to_string()))
            .email(Some("test@test.com".to_string()))
            .username(Some("myuser".to_string()))
            .build();
        let meta: UserMeta = r.ok().unwrap();
        assert!(meta.id.unwrap().eq("myid"));
        assert!(meta.email.unwrap().eq("test@test.com"));
        assert!(meta.username.unwrap().eq("myuser"));
    }

    #[test]
    fn should_serialize_user(){
        let r = UserMetaBuilder::default()
            .id(Some("myid".to_string()))
            .username(Some("myuser".to_string()))
            .build();
        let meta: UserMeta = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.ok().unwrap());
    }
}
