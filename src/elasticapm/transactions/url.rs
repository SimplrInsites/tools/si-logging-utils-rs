//! Url for a request
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;
use url::{Url, ParseError};


#[derive(Clone, Serialize)]
pub struct TransactionURL{
    #[serde(skip_serializing_if = "Option::is_none")]
    raw: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    protocol: Option<String>,
}


impl TransactionURL{

    pub fn new(raw_url: String) -> TransactionURL{

        TransactionURL{
            raw: Some(raw_url),
            protocol: None
        }
    }
}
