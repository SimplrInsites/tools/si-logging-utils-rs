//! Transaction builder
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;

use crate::elasticapm::transactions::counts::TransactionSpanCount;


#[derive(Builder, Clone, Serialize)]
struct APMTransaction{
    id: String,
    trace_id: String,
    span_count: TransactionSpanCount,
    duration: f64,
    #[serde(rename(serialize="type"))]
    transaction_type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    parent_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    result: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    marks: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    sampled: Option<bool>
}
