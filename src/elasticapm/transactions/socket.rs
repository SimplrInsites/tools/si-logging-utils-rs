//! Socket information for a request
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;


/// Request Socket
#[derive(Builder, Clone, Serialize)]
pub struct RequestSocket{
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_encrypted_default()")]
    encrypted: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_remote_address_default()")]
    remote_address: Option<String>
}


impl RequestSocketBuilder{

    fn get_encrypted_default(&self) -> Option<bool>{
        None
    }

    fn get_remote_address_default(&self) -> Option<String>{
        None
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_serialize_socket(){
        let r = RequestSocketBuilder::default()
            .encrypted(Some(true))
            .remote_address(Some("localhost".to_string()))
            .build();
        let s = r.ok().unwrap();
        let jstr = serde_json::to_string(&s);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}