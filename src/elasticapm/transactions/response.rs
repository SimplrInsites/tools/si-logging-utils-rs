//! Response data for a transaction
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;
use serde_json::Value;
use std::collections::HashMap;


#[derive(Builder, Clone, Serialize)]
pub struct TransactionResponse{
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_bool_default()")]
    finished: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_headers_default()")]
    headers: Option<HashMap<String, Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_bool_default()")]
    headers_sent: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_status_default()")]
    status_code: Option<i32>
}


/// Builder information
impl TransactionResponseBuilder{

    /// Get the boolean option default, None
    fn get_bool_default(&self) -> Option<bool>{
        None
    }

    /// Get the headers default, none
    fn get_headers_default(&self) -> Option<HashMap<String, Value>>{
        None
    }

    /// Get the status default, None
    fn get_status_default(&self) -> Option<i32>{
        None
    }
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_serialize_with_data(){
        let r = TransactionResponseBuilder::default()
            .finished(Some(false))
            .build();
        let resp = r.ok().unwrap();
        let jstr = serde_json::to_string(&resp);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}
