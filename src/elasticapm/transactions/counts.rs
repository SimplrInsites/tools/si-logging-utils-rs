//! Count information
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;


/// Transaction span count
#[derive(Builder, Clone, Serialize)]
pub struct TransactionSpanCount{
    started: i32,
    dropped: i32
}


#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn should_serialize_transaction_count(){
        let r = TransactionSpanCountBuilder::default()
            .started(0)
            .dropped(0)
            .build();
        assert!(r.is_ok());
        let meta = r.ok().unwrap();
        let jstr = serde_json::to_string(&meta);
        assert!(jstr.is_ok());
        println!("{}", jstr.unwrap());
    }
}
