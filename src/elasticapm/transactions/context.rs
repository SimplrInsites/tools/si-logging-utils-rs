//! Context for the transaction
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use serde::Serialize;
use serde_json::Value;

use crate::elasticapm::metadata::service::ServiceMeta;
use crate::elasticapm::transactions::response::TransactionResponse;
use crate::elasticapm::transactions::request::TransactionRequest;


#[derive(Builder, Clone, Serialize)]
struct APMContext{
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_service_default()")]
    service: Option<ServiceMeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_custom_default()")]
    custom: Option<HashMap<String,Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_request_default()")]
    request: Option<TransactionRequest>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_response_default()")]
    response: Option<TransactionResponse>
}


/// APM context implementation
impl APMContextBuilder{

    /// Get the transaction default, None
    fn get_request_default(&self) -> Option<TransactionRequest>{
        None
    }

    /// Get the response default, none
    fn get_response_default(&self) -> Option<TransactionResponse>{
        None
    }

    /// Get the service meta default, None
    fn get_service_default(&self) -> Option<ServiceMeta>{
        None
    }

    /// Get the custom default, none
    fn get_custom_default(&self) -> Option<HashMap<String, Value>>{
        None
    }
}
