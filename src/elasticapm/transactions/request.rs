//! Request information
//!
//! ---
//! author: Andrew Evans
//! ---

use serde::Serialize;
use crate::elasticapm::transactions::socket::RequestSocket;


/// Request information for the transaction
#[derive(Builder, Clone, Serialize)]
pub struct TransactionRequest{
    //url: TransactionURL,
    method: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default = "self.get_socket_default()")]
    socket: Option<RequestSocket>
}


impl TransactionRequestBuilder{

    /// Get the default request socket, none
    fn get_socket_default(&self) -> Option<RequestSocket>{
        None
    }
}
