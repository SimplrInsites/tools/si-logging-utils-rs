/// Configuration for elastic apm
///
/// ---
/// author: Andrew Evans
/// ---


/// Elastic APM Configuration
#[derive(Builder, Debug, PartialEq)]
struct ElasticAPMConfig{
    service_name: String,
    #[builder(default = "self.default_host()")]
    host: String,
    #[builder(default = "8200")]
    port: i32
}

impl ElasticAPMConfigBuilder{

    /// Get the default host. 127.0.0.1
    fn default_host(&self) -> String{
        "127.0.0.1".to_string()
    }
}

/// Elastic APM config implementation
impl ElasticAPMConfig{

    /// Get the service name string reference
    #[allow(dead_code)]
    pub fn get_service_name(&self) -> String{
        self.service_name.clone()
    }

    /// Get the host for the apm service. Should be localhost 127.0.0.1.
    #[allow(dead_code)]
    pub fn get_host(&self) -> String { self.host.clone()}

    /// Get the port for the application. Defaults to elasticapm default port.
    #[allow(dead_code)]
    pub fn get_port(&self) -> i32 { self.port}

}


#[cfg(test)]
mod test{
    use super::*;

    #[test]
    fn test_build_config(){
        let service_name = "test-service".to_string();
        let builder_res = ElasticAPMConfigBuilder::default()
            .service_name(service_name)
            .build();
        assert!(builder_res.is_ok());
        let config = builder_res.ok().unwrap();
        assert!(config.service_name.eq("test-service"))
    }
}
